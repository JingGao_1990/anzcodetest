
struct Summary {
    var gaining: Double
    var losing: Double

    init(gaining: Double?, losing: Double?) {
        self.gaining = gaining ?? 0.0
        self.losing = losing ?? 0.0
    }

}
