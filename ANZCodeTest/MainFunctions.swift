//
//  MainFunctions.swift
//  ANZCodeTest
//
//  Created by Jing Gao on 30/10/18.
//  Copyright © 2018 Jing Gao. All rights reserved.
//

import Foundation

class MainFunctions {
    
    //read and compute function
   static func readAndCompute(array: [Double]) -> Summary {
        var gaining: Double = 0.0
        var losing: Double = 0.0
        var result: Double = 0.0
        
        if (array.count == 0)||(array.count == 1)  {
            return Summary(gaining: 0.0, losing: 0.0)
        } else if array.count == 2 {
            result = array[1] - array[0]
            if result > 0 {
                return Summary(gaining: result, losing: 0.0)
            } else if result == 0 {
                return Summary(gaining: 0.0, losing: 0.0)
            } else {
                return Summary(gaining: 0.0, losing: result)
            }
            
        } else {
            for index in 0...(array.count - 1) {
                if index + 1 <= array.count - 1 {
                    result = array[index + 1] - array[index]
                    if result > 0 {
                        gaining = gaining + result
                        losing = losing + 0.0
                    } else if result < 0 {
                        losing = losing + result
                        gaining = gaining + 0.0
                    } else {
                        gaining = gaining + 0.0
                        losing = losing + 0.0
                    }
                }
            }
            return Summary(gaining: gaining.rounded(toPlaces: 2), losing: losing.rounded(toPlaces: 2))
        }
    }

}
