import Foundation

extension Double {
    // Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
}

extension String {
    //get input and convert to Double Array func
    func convertIntoArrayFromInput(input: String) -> [Double] {
        return input
            .components(separatedBy: ",")
            .compactMap {
                Double($0.trimmingCharacters(in: .whitespaces))
        }
        
    }
}
