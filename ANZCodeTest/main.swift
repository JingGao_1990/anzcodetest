//
//  main.swift
//  ANZCodeTest
//
//  Created by Jing Gao on 30/10/18.
//  Copyright © 2018 Jing Gao. All rights reserved.
//

import Foundation

print("Please enter numbers and separate them by comma, press Enter for start.")

let input = readLine() ?? ""

let array = input.convertIntoArrayFromInput(input: input)
let summary = MainFunctions.readAndCompute(array: array)
let gaining = summary.gaining
let losing = summary.losing

print("\(gaining)\n\(losing)")
